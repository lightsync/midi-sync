import { createTimesyncClient } from "../lib/timesync";
import { sendTimestamp } from "../lib/sendTimestamp";

export function getCurrentOffset(timesyncURL: string): Promise<number> {
  return new Promise(resolve => {
    const ts = createTimesyncClient(timesyncURL);

    let counter = 0;
    function onChange() {
      counter += 1;
      console.log(`Got sync ${counter}: ${ts.offset}`);

      if (counter === 3) {
        ts.off("change", onChange);
        ts.destroy();
        resolve(ts.offset);
      }
    }
    ts.on("change", onChange);
  });
}

export async function triggerSequence(opts: {
  name: string;
  timestamp: Date;
  cueWSAdminURL: string;
  timesyncURL: string;
}) {
  const offset = await getCurrentOffset(opts.timesyncURL);
  const timestamp = opts.timestamp.getTime() + offset;
  await sendTimestamp(opts.cueWSAdminURL, opts.name, timestamp);
  console.log(`Start timestamp for '${opts.name}' set to ${timestamp}`);
}

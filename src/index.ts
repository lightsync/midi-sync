import { createTimesyncClient } from "./lib/timesync";
import { Input } from "easymidi";
import { MTCStream, IMTCStreamOptions } from "@tim-smart/mtc";
import { Timecode } from "@tim-smart/timecode";
import { ITimesyncClient } from "timesync";
import { sendTimestamp } from "./lib/sendTimestamp";

interface INoteMessage {
  channel: number;
  note: number;
}

export interface ILightsyncMidiClientOptions {
  cueWSAdminURL: string;
  timesyncURL: string;
  midiDeviceName: string;

  // This is added to the timestamps to compensate for slow devices etc.
  latencyCompensation?: number;

  // Timecode options
  mtcOptions?: IMTCStreamOptions;

  sequences: ILightsyncSequenceMap;
}

export interface ILightsyncSequenceMap {
  [name: string]: ILightsyncSequenceMidiNote | ILightsyncSequenceTimecode;
}

export interface ILightsyncSequenceMidiNote {
  name?: string;
  type: "note";
  note: number;
  channel: number;
}

export interface ILightsyncSequenceTimecode {
  type: "timecode";

  hours: number;
  minutes: number;
  seconds: number;
  frames: number;

  name?: string;
}

export class LightsyncMidiClient {
  private ts: ITimesyncClient;
  private midiInput: Input;
  private mtc: MTCStream;
  private options: Required<ILightsyncMidiClientOptions>;
  private handledMTCChange: boolean = true;
  private tcSequences: ILightsyncSequenceTimecode[];
  private currentTCSequence?: ILightsyncSequenceTimecode;

  constructor(opts: ILightsyncMidiClientOptions) {
    this.options = {
      // Defaults
      latencyCompensation: 0,
      mtcOptions: {
        ...(opts.mtcOptions || {}),
      },

      ...opts,
    };

    this.ts = createTimesyncClient(opts.timesyncURL);
    this.midiInput = new Input(opts.midiDeviceName);
    this.mtc = new MTCStream(this.options.mtcOptions);

    this.tcSequences = Object.keys(opts.sequences)
      .map(name => {
        const seq = opts.sequences[name];
        seq.name = name;
        return opts.sequences[name];
      })
      .filter(
        sequence => sequence.type === "timecode",
      ) as ILightsyncSequenceTimecode[];

    this.tcSequences.sort((a, b) => {
      const aTC = new Timecode(a, this.mtc.timecodeOptions);
      const bTC = new Timecode(a, this.mtc.timecodeOptions);
      return aTC.frameCount() - bTC.frameCount();
    });

    this.midiInput.on("noteon", this.handleMidiNoteOn);
    this.ts.on("change", this.handleTimesyncChange);

    this.mtc.on("data", this.handleMTCData);
    this.midiInput._input.on("message", (delta: number, arr: number[]) => {
      const buf = Buffer.from(arr);
      this.mtc.write(buf);
    });
  }

  public destroy() {
    this.ts.destroy();
    this.midiInput.close();
  }

  private handleMidiNoteOn = (msg: INoteMessage) => {
    console.log("Got msg", msg);

    const { sequences } = this.options;

    Object.keys(sequences).forEach(name => {
      const sequence = sequences[name];
      if (sequence.type !== "note") {
        return;
      }

      if (msg.note !== sequence.note || msg.channel !== sequence.channel) {
        return;
      }

      this.triggerSequence(name);
    });
  };

  private async triggerSequence(name: string, offset: number = 0) {
    const now = this.ts.now() + this.options.latencyCompensation + offset;

    console.log("================================================");
    console.log(`==== SETTING '${name}' START TIMESTAMP: ${now}`);
    console.log("================================================");

    await sendTimestamp(this.options.cueWSAdminURL, name, now);
  }

  private handleTimesyncChange = () => {
    const tc = this.mtc.getCurrentTimecode();

    console.log(`Got time sync: ${this.ts.offset}.`);
    console.log(
      `MTC: ${tc.toString()} @ ${this.mtc.timecodeOptions.framerate}fps`,
    );

    this.handledMTCChange = false;
    this.handleMTCData(this.mtc.currentTimecode);
  };

  private handleMTCData = (tc: Timecode) => {
    let sequence: ILightsyncSequenceTimecode | undefined;
    let sequenceTimecode: Timecode | undefined;

    for (const currentSequence of this.tcSequences) {
      sequenceTimecode = new Timecode(
        currentSequence,
        this.mtc.timecodeOptions,
      );

      if (sequenceTimecode.frameCount() <= tc.frameCount()) {
        sequence = currentSequence;
      }
    }

    if (!sequence || !sequenceTimecode) {
      return;
    } else if (sequence === this.currentTCSequence && this.handledMTCChange) {
      return;
    }
    this.handledMTCChange = true;
    this.currentTCSequence = sequence;

    const diff = tc.subtract(sequenceTimecode);
    this.triggerSequence(sequence.name!, -diff.toMilliseconds());
  };
}

export { triggerSequence } from "./tools/triggerSequence";

export default function create(opts: ILightsyncMidiClientOptions) {
  return new LightsyncMidiClient(opts);
}

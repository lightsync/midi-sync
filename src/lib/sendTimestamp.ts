import request from "request-promise";

export function sendTimestamp(url: string, name: string, timestamp: number) {
  return request.get({
    url,

    qs: {
      name,
      startTimestamp: Math.round(timestamp),
    },
  });
}

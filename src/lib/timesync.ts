import { create } from "timesync";
import SocketIO = require("socket.io-client");

export function createTimesyncClient(url: string) {
  const io = SocketIO(url, {
    transports: ["websocket"],
  });

  const ts = create({
    interval: 10000,
    server: io,
  });
  ts._isFirst = false;

  ts.send = (socket, data, timeout) => {
    return new Promise((resolve, reject) => {
      const timeoutFn = setTimeout(reject, timeout);
      socket.emit("timesync", data, () => {
        clearTimeout(timeoutFn);
        resolve();
      });
    });
  };

  io.on("timesync", (data: any) => {
    ts.receive(null, data);
  });

  return ts;
}

declare module "timesync" {
  import { EventEmitter } from "events";

  interface ITimesyncClient extends EventEmitter {
    now(): number;
    offset: number;
    _isFirst: boolean;
    send(
      socket: SocketIOClient.Socket,
      data: any,
      timeout: number,
    ): Promise<void>;
    receive(err: Error | null, data: any): void;
    destroy(): void;
  }

  export const create: (opts?: any) => ITimesyncClient;
}

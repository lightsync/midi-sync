declare module "easymidi" {
  import { EventEmitter } from "events";

  export interface ISysexMessage {
    bytes: Buffer;
  }

  export class Input extends EventEmitter {
    public _input: EventEmitter;
    constructor(name: string);
    public close(): void;
  }

  export function getInputs(): string[];
}
